package main.java.repository;

import main.java.domain.Alexa_crypto_fact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Alexa_crypto_fact entity.
 */
@SuppressWarnings("unused")
@Repository
public interface Alexa_crypto_factRepository extends JpaRepository<Alexa_crypto_fact, Long> {

}
