package main.java.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.github.jhipster.web.util.ResponseUtil;
import main.java.domain.Alexa_crypto_fact;
import main.java.repository.Alexa_crypto_factRepository;
import main.java.web.rest.errors.BadRequestAlertException;
import main.java.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Alexa_crypto_fact.
 */
@RestController
@RequestMapping("/api")
public class Alexa_crypto_factResource {

    private final Logger log = LoggerFactory.getLogger(Alexa_crypto_factResource.class);

    private static final String ENTITY_NAME = "alexa_crypto_fact";

    private final Alexa_crypto_factRepository alexa_crypto_factRepository;

    public Alexa_crypto_factResource(Alexa_crypto_factRepository alexa_crypto_factRepository) {
        this.alexa_crypto_factRepository = alexa_crypto_factRepository;
    }

    @GetMapping("/alexa-crypto-facts-count")
    @Timed
    public ResponseEntity countAlexa_crypto_facts() {

       long count = alexa_crypto_factRepository.count();

       return ResponseEntity.ok(count);
    }

}
